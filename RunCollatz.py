#!/usr/bin/env python3
""" This script runs collatz using standard in and standard out as inputs and outputs """

# -------------
# RunCollatz.py
# -------------

# -------
# imports
# -------

from sys import stdin, stdout
from Collatz import collatz_solve

# ----
# main
# ----

if __name__ == "__main__":
    collatz_solve(stdin, stdout)
