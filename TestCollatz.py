#!/usr/bin/env python3
""" This file tests the collatz conjecture """

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    """ This is the test class that contains unit tests """

    # ----
    # read
    # ----

    def test_read(self):
        """ Tests the read function """
        test_string = "1 10\n"
        first, second = collatz_read(test_string)
        self.assertEqual(first, 1)
        self.assertEqual(second, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """ Tests the eval function """
        value = collatz_eval(1, 10)
        self.assertEqual(value, 20)

    def test_eval_2(self):
        """ Tests the eval function """
        value = collatz_eval(100, 200)
        self.assertEqual(value, 125)

    def test_eval_3(self):
        """ Tests the eval function """
        value = collatz_eval(201, 210)
        self.assertEqual(value, 89)

    def test_eval_4(self):
        """ Tests the eval function """
        value = collatz_eval(900, 1000)
        self.assertEqual(value, 174)

    def test_eval_5(self):
        """ Tests the eval function """
        value = collatz_eval(1000, 3000)
        self.assertEqual(value, 217)

    def test_eval_6(self):
        """ Tests the eval function """
        value = collatz_eval(1, 1)
        self.assertEqual(value, 1)

    def test_eval_7(self):
        """ Tests the eval function """
        value = collatz_eval(1, 999999)
        self.assertEqual(value, 525)

    def test_eval_8(self):
        """ Tests the eval function """
        value = collatz_eval(1, 3001)
        self.assertEqual(value, 217)

    def test_eval_9(self):
        """ Tests the eval function """
        value = collatz_eval(1, 2001)
        self.assertEqual(value, 182)

    def test_eval_10(self):
        """ Tests the eval function """
        value = collatz_eval(506697, 61596)
        self.assertEqual(value, 449)

    def test_eval_11(self):
        """ Tests the eval function """
        value = collatz_eval(879198, 215976)
        self.assertEqual(value, 525)

    def test_eval_12(self):
        """ Tests the eval function """
        value = collatz_eval(191111, 155496)
        self.assertEqual(value, 383)

    def test_eval_13(self):
        """ Tests the eval function """
        value = collatz_eval(173940, 3043)
        self.assertEqual(value, 383)

    def test_eval_14(self):
        """ Tests the eval function """
        value = collatz_eval(168596, 4379)
        self.assertEqual(value, 383)

    # -----
    # print
    # -----

    def test_print(self):
        """ Tests the print function """
        write = StringIO()
        collatz_print(write, 1, 10, 20)
        self.assertEqual(write.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """ Tests the solve function """
        read = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        write = StringIO()
        collatz_solve(read, write)
        self.assertEqual(
            write.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
