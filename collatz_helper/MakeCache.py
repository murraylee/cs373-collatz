# Makes cache
from Collatz import collatz_eval
from typing import List

# ------------
# make_cache
# ------------
def make_cache(size: int = 1000, largest=1000000) -> List[int]:
    # return [collatz_eval(i*1000 + 1, (i+1)*1000) for i in range(size)]
    cache = []
    for i in range(size):
        dist = largest // size
        ans = collatz_eval(i * dist, (i + 1) * dist - 1)
        print(i, ans)
        cache.append(ans)
    return cache


def main():
    cache = make_cache()
    with open("cache.txt", "w") as f:
        f.write(str(cache))


main()
